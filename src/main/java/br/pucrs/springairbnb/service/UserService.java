package br.pucrs.springairbnb.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.pucrs.springairbnb.dto.UserDTO;
import br.pucrs.springairbnb.model.Imovel;
import br.pucrs.springairbnb.model.User;
import br.pucrs.springairbnb.repository.UserRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class UserService {
    
    private UserRepository repoUs;

    public User cadastraUser(UserDTO us){
        User usModel = null;

        if (us != null) {
            Optional<User> aux = repoUs.findById(us.getUserId());
            aux.ifPresent(user1 -> {
                                    throw new IllegalArgumentException("Usuário existente!");
                                   });
            
            usModel = new User(us.getUserId(), us.getSenha(), us.getTipo(), new ArrayList<Imovel>()); 
            repoUs.insert(usModel);
        }
        
        return usModel;
    }
}
