package br.pucrs.springairbnb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.pucrs.springairbnb.dto.UserDTO;
import br.pucrs.springairbnb.service.UserService;
import br.pucrs.springairbnb.util.TipoUser;

@SpringBootApplication
public class SpringairbnbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringairbnbApplication.class, args);
	}

	// @Bean
	// public CommandLineRunner demo(UserService cl) {
	// 	return (args) -> {
			
	// 		cl.cadastraUser(new UserDTO("jj1", "senha1", TipoUser.LOCADOR));
	// 		cl.cadastraUser(new UserDTO("jj2", "senha2", TipoUser.LOCADOR));
	// 		cl.cadastraUser(new UserDTO("jj3", "senha3", TipoUser.LOCATARIO));

	// 	};
	// }
}
