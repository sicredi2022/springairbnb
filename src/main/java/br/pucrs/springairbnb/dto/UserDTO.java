package br.pucrs.springairbnb.dto;

import br.pucrs.springairbnb.util.TipoUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private String userId;
    private String senha;
    private TipoUser tipo;
}
