package br.pucrs.springairbnb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.springairbnb.model.Imovel;


public interface ImovelRepository extends MongoRepository<Imovel, String> {
    public Imovel findByApelidoImovel(String ap);
}