package br.pucrs.springairbnb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.springairbnb.model.Imovel;
import br.pucrs.springairbnb.model.User;


public interface UserRepository extends MongoRepository<User, String> {
    public Imovel findByUserId(String us);
}