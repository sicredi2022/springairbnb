package br.pucrs.springairbnb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.springairbnb.model.Logado;


public interface LogadoRepository extends MongoRepository<Logado, String> {
}