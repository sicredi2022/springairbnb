package br.pucrs.springairbnb.model;

import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;

@Data
@Document(collection = "logado")
public class Logado {
    private User logado;
}
