package br.pucrs.springairbnb.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import br.pucrs.springairbnb.util.TipoUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User {
    @Id
    private String userId;
    private String senha;
    private TipoUser tipo;
    private List<Imovel> alugados;

    public void addAlugados(Imovel im) {
        if (im != null)
            alugados.add(im);
    }
}
