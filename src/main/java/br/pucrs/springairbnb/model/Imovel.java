package br.pucrs.springairbnb.model;

import org.springframework.data.mongodb.core.mapping.Document;

import br.pucrs.springairbnb.util.TipoAp;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection="imoveis")
public class Imovel {
    private String _id;
    private String apelidoImovel;
    private String descricao;
    private TipoAp tipo;
    private String cidade;
    private double preço;
}
