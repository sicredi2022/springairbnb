package br.pucrs.springairbnb.util;

public enum TipoAp {
    AP, CASA, QUARTO;
}
