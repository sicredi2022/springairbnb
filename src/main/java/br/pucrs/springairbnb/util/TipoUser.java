package br.pucrs.springairbnb.util;

public enum TipoUser {
    LOCADOR, LOCATARIO;
}
