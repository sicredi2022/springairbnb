package br.pucrs.springairbnb.controller;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.pucrs.springairbnb.dto.UserDTO;
import br.pucrs.springairbnb.model.User;
import br.pucrs.springairbnb.service.UserService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class UserController {
    private UserService uService;

    @PostMapping("/user/add")
	public ResponseEntity<Void> cadastraUsEntity(@RequestBody UserDTO newUser) {

		User us = uService.cadastraUser(newUser);

		if (us == null)
			return ResponseEntity.noContent().build();

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().
                            buildAndExpand(us.getUserId()).toUri();

		return ResponseEntity.created(location).build();
	}

}
