package br.pucrs.springairbnb.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.pucrs.springairbnb.dto.UserDTO;
import br.pucrs.springairbnb.repository.UserRepository;
import br.pucrs.springairbnb.util.TipoUser;
import br.pucrs.springairbnb.model.User;
import br.pucrs.springairbnb.model.Imovel;


public class UserServiceMockTest {
    
    private UserService uService;
    private UserRepository mockRepo;
    
    @BeforeEach
    public void setUP() {
        mockRepo = mock(UserRepository.class);
        uService = new UserService(mockRepo);
    }

    @Test
    public void cadastraUserInseriTest(){
        UserDTO usDto = new UserDTO("jj1", "senha1", TipoUser.LOCADOR);;
        User usRes, usExpect;
        
        when(mockRepo.findById(anyString())).thenReturn(Optional.empty());

        
        usExpect = new User(usDto.getUserId(), usDto.getSenha(), usDto.getTipo(), new ArrayList<Imovel>());
        usRes = uService.cadastraUser(usDto);

        assertEquals(usExpect, usRes);
    }

    @Test
    public void cadastraUserExcpTest(){
        UserDTO usDto = new UserDTO("jj1", "senha1", TipoUser.LOCADOR);;
        User us;

        us = new User(usDto.getUserId(), usDto.getSenha(), usDto.getTipo(), new ArrayList<Imovel>());
        
        when(mockRepo.findById(anyString())).
                                 thenReturn(
                                    Optional.of(us));

        
        assertThrows(IllegalArgumentException.class, () -> uService.cadastraUser(usDto));
    }
}
