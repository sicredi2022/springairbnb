// package br.pucrs.springairbnb.service;

// import static org.junit.jupiter.api.Assertions.assertEquals;
// import static org.junit.jupiter.api.Assertions.assertThrows;
// import static org.mockito.ArgumentMatchers.anyString;
// import static org.mockito.Mockito.mock;
// import static org.mockito.Mockito.when;

// import java.util.ArrayList;
// import java.util.Optional;

// import org.junit.jupiter.api.BeforeAll;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.TestInstance;
// import org.springframework.beans.factory.annotation.Autowired;

// import br.pucrs.springairbnb.dto.UserDTO;
// import br.pucrs.springairbnb.repository.UserRepository;
// import br.pucrs.springairbnb.util.TipoUser;
// import br.pucrs.springairbnb.model.User;
// import br.pucrs.springairbnb.model.Imovel;

// @TestInstance(TestInstance.Lifecycle.PER_CLASS)
// public class UserServiceIntegrationTest {
    
//     private UserService uService;
//     @Autowired
//     private UserRepository repoUs;
    
//     @BeforeAll
//     public void fixture() {
//         uService = new UserService(repoUs);
//     }

//     @BeforeEach
//     public void setUp() {
//         repoUs.deleteAll();
//     }

//     @Test
//     public void cadastraUserInseriTest(){
//         UserDTO usDto = new UserDTO("jj1", "senha1", TipoUser.LOCADOR);;
//         User usRes, usExpect;
             
//         usExpect = new User(usDto.getUserId(), usDto.getSenha(), usDto.getTipo(), new ArrayList<Imovel>());
//         usRes = uService.cadastraUser(usDto);

//         assertEquals(usExpect, usRes);
//     }

//     @Test
//     public void cadastraUserExcpTest(){
//         UserDTO usDto = new UserDTO("jj1", "senha1", TipoUser.LOCADOR);;
//         User us;

//         us = new User(usDto.getUserId(), usDto.getSenha(), usDto.getTipo(), new ArrayList<Imovel>());
//         us = uService.cadastraUser(usDto);

//         assertThrows(IllegalArgumentException.class, () -> uService.cadastraUser(usDto));
//     }
// }
